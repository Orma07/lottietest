﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;


namespace LottieTest
{
    public partial class MainPage : ContentPage
    {
        public ICommand PlayingCommand { get; private set; }
        public ICommand FinishedCommand { get; private set; }
        public ICommand ClickedCommand { get; private set; }

        private List<Color> colors;
        private int index = 0;

        public MainPage()
        {
           

            InitializeComponent();

            colors = new List<Color>(){ Color.Red, Color.Purple, Color.Peru, Color.White, Color.Violet, Color.Yellow, Color.Black};

            playButton.Clicked += (sender, e) => animationView.Play();
            changeBackgroundColorButton.Clicked += ChangeBackgroundColorButton;
            pauseButton.Clicked += (sender, e) => animationView.Pause();

            BindingContext = this;

            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        private void ChangeBackgroundColorButton(object sender, EventArgs e)
        {
            index++;
            if (index > colors.Count)
            {
                index = 0;
            }
            animationView.BackgroundColor = colors[index];
        }

        

        private void Slider_OnValueChanged(object sender, ValueChangedEventArgs e)
        {
            animationView.Progress = (float)e.NewValue;
        }

        private void Handle_OnFinish(object sender, System.EventArgs e)
        {
            
        }

        private void DisplayAlert(string message)
        {
           
        }
    }
}

**Appunto:**

Lottie in xamarin forms permette grazie a un player json di mostrare animazioni esportate con bodymovin. Utilizzandolo in xamarin forms si puo principalmente manipolare le fasi dell’animazione (start, stop, pausa e velocità…), ci sono invece dei limiti sulla manipolazione dei colori dei vari layer dell’animazione (i layer equivalgono a quelli presenti in addobe after effects) però possibile a livello nativo nelle singole piattaforme (Xamarin.Android, Xamarin.UWP e Xamarin.iOs). 
Quindi per l'utilizzo del plugin in By-cloud proporrei di avere già un set di animazione esportate con la forma finale, quindi senza cambiare dinamicamente alcuna proprietà.



**Osservazione memoria e performance:**



Se la composizione non ha maschere o mascherini, le prestazioni e il sovraccarico della memoria dovrebbero essere buone. Non vengono create bitmap e la maggior parte delle operazioni sono semplici operazioni di disegno su canvans. Se la composizione ha mascherini, vengono creati 2-3 bitmap, le bitmap consumano molta RAM per questo motivo, non è consigliabile utilizzare animazioni con maschere o mascherini, oltre alla perdita di memoria, sono necessarie ulteriori chiamate bitmap.eraseColor () e canvas.drawBitmap () per maschere e mascherini che rallentano le prestazioni dell'animazione. 

Se si utilizza l'animazione in una lista, si consiglia di utilizzare CacheStrategy in LottieAnimationView.setAnimation (String, CacheStrategy) in modo che le animazioni non debbano essere deserializzate ogni volta.
